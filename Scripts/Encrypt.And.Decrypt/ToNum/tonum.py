#!/usr/bin/env python3

import inflect

def number_to_words(number):
    p = inflect.engine()
    return p.number_to_words(number)

number = input("Enter a number: ")
words = number_to_words(number)
print(words)
