#!/bin/bash

# Function to encrypt the message using Scytale
function encrypt_scytale() {
    local message="$1"
    local scytale_diameter=8

    # Calculate the number of spaces to add for padding
    local num_spaces=$((scytale_diameter - (${#message} % scytale_diameter) % scytale_diameter))

    # Pad the message by adding spaces at the end
    local padded_message="${message}$(printf '%*s' $num_spaces)"

    # Calculate the number of wraps required
    local num_wraps=$(((${#padded_message} + scytale_diameter - 1) / scytale_diameter))

    # Initialize the ciphertext
    local ciphertext=""

    # Loop through each column and concatenate characters from each wrap
    for ((i = 0; i < scytale_diameter; i++)); do
        for ((j = 0; j < num_wraps; j++)); do
            local index=$((i + j * scytale_diameter))
            ciphertext+="${padded_message:index:1}"
        done
    done

    echo "$ciphertext"
}

# Check if input file exists
if [ -f "$1" ]; then
    input_file="$1"
else
    echo "Error: Input file not found."
    exit 1
fi

# Check if output file is provided
if [ -n "$2" ]; then
    output_file="$2"
else
    echo "Error: Please provide an output file name."
    exit 1
fi

# Read the message from the input file
plaintext=$(cat "$input_file")

# Encrypt the message using Scytale
encrypted_message=$(encrypt_scytale "$plaintext")

# Trim any trailing spaces or padding characters from the encrypted message
encrypted_message=$(echo "$encrypted_message" | sed 's/[[:space:]]*$//')

# Write the encrypted message to the output file
echo "$encrypted_message" > "$output_file"

echo "Encryption completed. The encrypted message has been written to $output_file."

