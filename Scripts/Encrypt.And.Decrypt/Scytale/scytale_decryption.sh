#!/bin/bash

# Function to decrypt the message using Scytale
function decrypt_scytale() {
    local ciphertext="$1"
    local scytale_diameter=8

    # Calculate the number of wraps required based on the original ciphertext length
    local num_wraps=$(( (${#ciphertext} + scytale_diameter - 1) / scytale_diameter ))

    # Initialize the plaintext
    local plaintext=""

    # Loop through each wrap and concatenate characters from each column
    for ((j = 0; j < num_wraps; j++)); do
        for ((i = 0; i < scytale_diameter; i++)); do
            local index=$((i * num_wraps + j))
            plaintext+="${ciphertext:index:1}"
        done
    done

    # Trim any trailing spaces or padding characters from the plaintext
    plaintext=$(echo "$plaintext" | sed 's/[[:space:]]*$//')

    echo "$plaintext"
}


# Check if input file exists
if [ -f "$1" ]; then
    input_file="$1"
else
    echo "Error: Input file not found."
    exit 1
fi

# Check if output file is provided
if [ -n "$2" ]; then
    output_file="$2"
else
    echo "Error: Please provide an output file name."
    exit 1
fi

# Read the encrypted message from the input file
ciphertext=$(cat "$input_file")

# Decrypt the message using Scytale
decrypted_message=$(decrypt_scytale "$ciphertext")

# Write the decrypted message to the output file
echo "$decrypted_message" > "$output_file"

echo "Decryption completed. The decrypted message has been written to $output_file."

