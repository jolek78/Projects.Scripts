#!/bin/bash

# Function to decrypt a single character using the Atbash cipher
atbash_decrypt_char() {
    char="$1"
    if [[ "$char" =~ [A-Za-z] ]]; then
        ascii_value=$(printf "%d" "'$char")
        if [[ "$char" =~ [A-Z] ]]; then
            new_ascii_value=$((90 - ascii_value + 65))
        else
            new_ascii_value=$((122 - ascii_value + 97))
        fi
        decrypted_char=$(printf \\$(printf "%03o" "$new_ascii_value"))
    else
        decrypted_char="$char"
    fi
    echo -n "$decrypted_char"
}

# Function to decrypt the entire input string using Atbash cipher
atbash_decrypt() {
    input="$1"
    decrypted=""
    for (( i=0; i<${#input}; i++ )); do
        decrypted+=`atbash_decrypt_char "${input:i:1}"`
    done
    echo "$decrypted"
}

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 input_file output_file"
    exit 1
fi

input_file="$1"
output_file="$2"

# Check if the input file exists and is readable
if [ ! -r "$input_file" ]; then
    echo "Error: Input file '$input_file' not found or is not readable."
    exit 1
fi

# Read the content of the input file
input_text=$(<"$input_file")

# Decrypt the input text using Atbash cipher
decrypted_text=$(atbash_decrypt "$input_text")

# Write the decrypted text to the output file
echo "$decrypted_text" > "$output_file"

echo "Decryption complete. Decrypted data written to '$output_file'."

