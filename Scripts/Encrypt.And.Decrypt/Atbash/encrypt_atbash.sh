#!/bin/bash

# Function to encrypt a single character using the Atbash cipher
atbash_encrypt_char() {
    char="$1"
    if [[ "$char" =~ [A-Za-z] ]]; then
        ascii_value=$(printf "%d" "'$char")
        if [[ "$char" =~ [A-Z] ]]; then
            new_ascii_value=$((90 - ascii_value + 65))
        else
            new_ascii_value=$((122 - ascii_value + 97))
        fi
        encrypted_char=$(printf \\$(printf "%03o" "$new_ascii_value"))
    else
        encrypted_char="$char"
    fi
    echo -n "$encrypted_char"
}

# Function to encrypt the entire input string using Atbash cipher
atbash_encrypt() {
    input="$1"
    encrypted=""
    for (( i=0; i<${#input}; i++ )); do
        encrypted+=`atbash_encrypt_char "${input:i:1}"`
    done
    echo "$encrypted"
}

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 input_file output_file"
    exit 1
fi

input_file="$1"
output_file="$2"

# Check if the input file exists and is readable
if [ ! -r "$input_file" ]; then
    echo "Error: Input file '$input_file' not found or is not readable."
    exit 1
fi

# Read the content of the input file
input_text=$(<"$input_file")

# Encrypt the input text using Atbash cipher
encrypted_text=$(atbash_encrypt "$input_text")

# Write the encrypted text to the output file
echo "$encrypted_text" > "$output_file"

echo "Encryption complete. Encrypted data written to '$output_file'."

