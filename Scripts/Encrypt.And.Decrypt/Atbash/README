ATBASH CYPHER - IN BASH
This is a basic project to encrypt and decrypt a text file using the atbash cyper, an ancient encryption system that was originally used in the Hebrew language. The cipher is named "Atbash" because it comes from the first Hebrew letter Aleph and the last Taff.

HOW DOES IT WORK?
It is a simple substitution cipher that relies on transposing all the letters in the alphabet so that the resulting alphabet is backwards. Each letter in the plaintext is replaced with its corresponding letter in the reversed alphabet. For example, the letter 'A' is replaced with 'Z', 'B' with 'Y', and so on.

Here is an example:


Plain:  ABCDEFGHIJKLMNOPQRSTUVWXYZ
Cipher: ZYXWVUTSRQPONMLKJIHGFEDCBA


RUNNING THE SCRIPT(S)
chmod +x encrypt_atbash.sh decrypt_atbash.sh


./encrypt_atbash.sh lorem-ipsum.txt lorem-ipsum--encrypt.txt
Encryption complete. Encrypted data written to 'lorem-ipsum--encrypt.txt'.


./decrypt_atbash.sh lorem-ipsum--encrypt.txt lorem-ipsum--decrypt.txt
Decryption complete. Decrypted data written to 'lorem-ipsum--decrypt.txt'.


diff -s lorem-ipsum--decrypt.txt lorem-ipsum.txt
Files lorem-ipsum--decrypt.txt and lorem-ipsum.txt are identical
