#!/usr/bin/python3

# NOT operator

print('+---+---+-------+')
print('| A | B | A || B|')
print('+---+---+-------+')
print('| 0 | 0 |   0   |')
print('| 0 | 1 |   1   |')
print('| 1 | 0 |   1   |')
print('| 1 | 1 |   1   |')
print('+---+---+-------+')

