#!/usr/bin/env python

def load_key(key_file):
    with open(key_file, 'r') as file:
        key_lines = file.readlines()

    # Remove the first cell (identifier) and parse the key values
    key = [line.strip().split()[1:] for line in key_lines]
    key = [list(map(int, line)) for line in key]

    return key


def encrypt(plaintext, key):
    encrypted = ''
    key_index = 0

    for char in plaintext:
        if char.isalpha():
            # Convert the character to its ASCII value and XOR with the corresponding key value
            encrypted_char = chr(ord(char) ^ key[key_index // 5][key_index % 5])
            encrypted += encrypted_char

            key_index += 1

            # If we have used all key values, reset the index to 0 to start from the beginning
            if key_index == len(key) * len(key[0]):
                key_index = 0
        else:
            # Non-alphabetic characters are left unchanged
            encrypted += char

    return encrypted


def main():
    key_file = 'one_time-output.txt'
    plaintext_file = 'lorem-ipsum.txt'
    output_file = 'lorem-ipsum-enc.txt'

    key = load_key(key_file)

    with open(plaintext_file, 'r') as file:
        plaintext = file.read()

    encrypted_text = encrypt(plaintext, key)

    with open(output_file, 'w') as file:
        file.write(encrypted_text)

    print("Encryption completed. Encrypted text saved to 'lorem-ipsum-enc.txt'")


if __name__ == "__main__":
    main()

