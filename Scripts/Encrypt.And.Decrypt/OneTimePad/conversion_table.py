#!/usr/bin/env python 

def letter_to_number_table():
    print("Letter to Number Conversion Table")
    print("--------------------------------")
    print("Capital Letters:")
    for letter in range(ord('A'), ord('Z') + 1):
        print(f"{chr(letter)}: {letter - ord('A') + 1}", end=' | ')
    print("\n--------------------------------")
    print("Lowercase Letters:")
    for letter in range(ord('a'), ord('z') + 1):
        print(f"{chr(letter)}: {letter - ord('a') + 1}", end=' | ')

if __name__ == "__main__":
    letter_to_number_table()

