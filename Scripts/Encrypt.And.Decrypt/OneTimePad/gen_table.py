#!/usr/bin/env python3
import random

def generate_random_table(lines, columns):
    table = []
    for _ in range(lines):
        row = []
        for _ in range(columns):
            cell = "".join(str(random.randint(0, 9)) for _ in range(5))
            row.append(cell)
        table.append(row)
    return table

def write_table_to_file(table, file_path):
    with open(file_path, 'w') as file:
        for row in table:
            file.write(" ".join(cell for cell in row) + "\n")

def main():
    lines = 10
    columns = 5

    random_table = generate_random_table(lines, columns)

    file_path = "one_time-output.txt"
    write_table_to_file(random_table, file_path)

if __name__ == "__main__":
    main()
