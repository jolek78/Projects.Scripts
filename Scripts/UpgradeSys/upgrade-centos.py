#!/usr/bin/env python

import subprocess

# Function to display a message and run a command
def run_command_with_message(message, command):
    print(message)
    subprocess.call(command, shell=True)

# Display a message
print("Checking for upgradable packages...")

# Check for upgradable packages using 'yum list updates'
result = subprocess.check_output("sudo yum list updates", shell=True, universal_newlines=True)

# Print the upgradable packages
print(result)

# Check if there are kernel updates
if "kernel" in result:
    # Ask if the user wants to run 'yum update' for the kernel
    update_kernel_choice = raw_input("A new kernel update is available. Do you want to update it? (yes/no): ").strip().lower()

    if update_kernel_choice == "yes":
        # Run 'yum update' with 'kernel' as an argument to update the kernel
        run_command_with_message("Running 'yum update' for the kernel...", "sudo yum update kernel")
    else:
        print("Skipping 'yum update' for the kernel.")
else:
    print("No kernel updates found.")

# Ask if the user wants to proceed with package upgrades (excluding the kernel)
upgrade_choice = raw_input("Do you want to manually update all other packages (excluding the kernel)? (yes/no): ").strip().lower()

if upgrade_choice == "yes":
    # Run 'yum update' to upgrade all packages except the kernel
    run_command_with_message("Updating all other packages (excluding the kernel)...", "sudo yum update -y --exclude=kernel*")
    print("Package upgrade completed (excluding the kernel).")
else:
    print("Package upgrade skipped.")

# Display a final message
print("Script completed.")
