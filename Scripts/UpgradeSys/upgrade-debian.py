#!/bin/python
import subprocess

# Function to display a message and run a command
def run_command_with_message(message, command):
    print(message)
    subprocess.run(command, shell=True)

# Display a message
print("Checking for upgradable packages...")

# Check for upgradable packages
result = subprocess.run("sudo apt list --upgradable", shell=True, capture_output=True, text=True)

# Print the upgradable packages
print(result.stdout)

# Check if there are kernel updates
if "linux-image" in result.stdout:
    # Ask if the user wants to update the kernel or all other packages
    update_choice = input("Do you want to update the kernel or all other packages? (kernel/other): ").strip().lower()

    if update_choice == "kernel":
        # Ask if the user wants to run apt update for the kernel
        kernel_update_choice = input("Do you want to run 'apt update' to update the kernel? (yes/no): ").strip().lower()

        if kernel_update_choice == "yes":
            # Run 'apt update' with the -y flag to automatically answer yes to prompts
            run_command_with_message("Running 'apt update' for the kernel...", "sudo apt update -y")
        else:
            print("Skipping 'apt update' for the kernel.")
    elif update_choice == "other":
        # Run 'apt upgrade' with the -y flag to automatically answer yes to prompts
        run_command_with_message("Upgrading all other packages...", "sudo apt upgrade -y")
        print("Package upgrade for all other packages completed.")

        # Ask if the user wants to run 'apt autoremove'
        autoremove_choice = input("Do you want to run 'apt autoremove' to remove unused packages? (yes/no): ").strip().lower()

        if autoremove_choice == "yes":
            # Run 'apt autoremove' with the -y flag to automatically answer yes to prompts
            run_command_with_message("Running 'apt autoremove'...", "sudo apt autoremove -y")
            print("Unused packages removed.")
        else:
            print("Skipping 'apt autoremove' for all other packages.")
    else:
        print("Invalid choice.")
else:
    print("No kernel updates found.")

# Display a final message
print("Script completed.")
