#!/usr/bin/python3

import subprocess

def run_command(command):
    result = subprocess.run(command, capture_output=True, text=True)
    if result.returncode == 0:
        return result.stdout.strip()
    else:
        return result.stderr.strip()

def choose_hardware_command():
    command_descriptions = {
        "lsblk": "List block devices",
        "lscpu": "Display CPU information",
        "lshw":  "List hardware information",
        "lspci": "List PCI devices",
        "lsusb": "List USB devices"
    }
    command = input("Choose a hardware command (lsblk, lscpu, lshw, lspci, lsusb): ")
    if command in command_descriptions:
        description = command_descriptions[command]
        print(f"Description: {description}\n")
        output = run_command(["echo", description])
        print(output)
        print("Executing command...")
        output = run_command([command])
        print(output)
    else:
        print("Invalid command")

choose_hardware_command()

