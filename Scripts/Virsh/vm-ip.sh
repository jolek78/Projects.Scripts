#!/bin/bash

# Get a list of running VM names
running_vms=$(virsh list | awk 'NR>2 {print $2}')

# Loop through each running VM and get its IP address
for vm_name in $running_vms; do
    ip_address=$(virsh domifaddr $vm_name | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}')
    if [ -n "$ip_address" ]; then
        echo "VM $vm_name IP: $ip_address"
    else
        echo "VM $vm_name has no assigned IP address."
    fi
done
