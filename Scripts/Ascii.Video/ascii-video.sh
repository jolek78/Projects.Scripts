#!/bin/bash

play_video() {
    mplayer -really-quiet -vo caca "$1" 2>/dev/null
}

# Function to generate tab-completions
_manual_tab_completion() {
    local completions=()
    while IFS= read -r -d $'\0'; do
        completions+=("$REPLY")
    done < <(compgen -G "${1}*" -o plusdirs -o bashdefault -o default -o filenames -o dirnames -o nospace -o filenames -X '!*.mkv' -X '!*.mp4' -X '!*.avi' -z)
    COMPREPLY=("${completions[@]}")
}

# Enable tab-completion for the 'play_video' command
complete -F _manual_tab_completion play_video

while true; do
    read -e -p "Enter the video file path (or 'exit' to quit): " video_file

    if [[ "$video_file" == "exit" ]]; then
        break
    fi

    if [[ -f "$video_file" ]]; then
        play_video "$video_file"
    else
        echo "Error: File '$video_file' not found."
    fi
done
