#!/usr/bin/python3
import subprocess
import sys

# A function that prompts the user for input and returns it
def get_input(prompt):
    return input(prompt)

# A function that prints a message and speaks it using the 'espeak' command
def print_and_speak(message):
    print(message)
    subprocess.run(["espeak", message])

# The main function that runs the game
def main():
    # Prompt the user to play the game
    play_game = get_input("Do you want to play? (yes/no) ")

    if play_game.lower() == "yes":
        # Prompt the user for their name
        name = get_input("Hello dude, what is your name? ")

        # Check if the name is a number
        if name.isdigit() or name.isnumeric():
            print("Please enter a valid input (not a number)")
            sys.exit()

        # Define different messages based on the user's name
        welcome_message   = f"Welcome {name}, happy hacking!"
        gates_message     = f"Bastard! You are not welcome here! Get out!"
        unwelcome_message = f"Hi {name}, I know you are crazy, but you can work!"
        random_message    = f"Hi {name}, have fun, and don't forget your towel!"

        # Check the user's name and print/speak the appropriate message
        if name.lower() == 'jolek78':
            print_and_speak(welcome_message)
        elif name.lower() == 'bill gates':
            print_and_speak(gates_message)
        elif name.lower() == 'satan':
            print_and_speak(unwelcome_message)
        else:
            print_and_speak(random_message)

    elif play_game.lower() == "no":
        print("Ok, maybe next time...")

    else:
        print("Make sure to reply yes/no")
        exit()

if __name__ == "__main__":
    main()
