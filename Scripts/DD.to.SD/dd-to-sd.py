#!/usr/bin/env python3

import sys
import subprocess
import os
import shutil
import tarfile
import zipfile
import gzip
import lzma

if sys.version_info[0] < 3:
    print("Error: This script requires Python 3. Please update your Python version.")
    sys.exit(1)

def check_and_install_dependencies():
    try:
        subprocess.run(["which", "dd"], check=True)
    except subprocess.CalledProcessError:
        print("Installing 'dd'...")
        subprocess.run(["sudo", "apt-get", "install", "coreutils"], check=True)

    try:
        subprocess.run(["which", "pv"], check=True)
    except subprocess.CalledProcessError:
        print("Installing 'pv'...")
        subprocess.run(["sudo", "apt-get", "install", "pv"], check=True)

def decompress_file(file_path):
    if file_path.endswith(('.tar.gz', '.gz', '.tgz', '.zip', '.xz')):
        temp_dir = 'temp_decompressed_files'
        os.makedirs(temp_dir, exist_ok=True)

        if file_path.endswith(('.tar.gz', '.tgz')):
            with tarfile.open(file_path, 'r:gz') as tar:
                for member in tar.getmembers():
                    print(f"Extracting {member.name}")
                    tar.extract(member, temp_dir)
        elif file_path.endswith('.gz'):
            output_file = os.path.join(temp_dir, os.path.splitext(os.path.basename(file_path))[0])
            print(f"Decompressing {file_path} to {output_file}")
            with gzip.open(file_path, 'rb') as f_in:
                with open(output_file, 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
        elif file_path.endswith('.zip'):
            with zipfile.ZipFile(file_path, 'r') as z:
                for member in z.infolist():
                    print(f"Extracting {member.filename}")
                    z.extract(member, temp_dir)
        elif file_path.endswith('.xz'):
            output_file = os.path.join(temp_dir, os.path.splitext(os.path.basename(file_path))[0])
            print(f"Decompressing {file_path} to {output_file}")
            with lzma.open(file_path) as f_in:
                with open(output_file, 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)

        print(f"Decompressed {file_path} to {temp_dir}")
        return temp_dir

    return None

def write_image_to_device(image_file, device):
    if not os.path.isfile(image_file):
        print(f"Error: {image_file} not found.")
        sys.exit(1)

    if not os.path.exists(device):
        print(f"Error: {device} not found.")
        sys.exit(1)

    temp_dir = decompress_file(image_file)
    if temp_dir:
        image_file = os.path.join(temp_dir, os.listdir(temp_dir)[0])

    try:
        size = os.path.getsize(image_file)
        with open(image_file, 'rb') as f:
            command_pv = ["pv", f"-s {size}"]
            pv_process = subprocess.Popen(command_pv, stdin=f, stdout=subprocess.PIPE)

            command_dd = [
                "sudo",
                "dd",
                f"of={device}",
                "bs=4M",
                "conv=fdatasync",
            ]
            dd_process = subprocess.Popen(command_dd, stdin=pv_process.stdout)
            pv_process.stdout.close()
            dd_process.communicate()

        print(f"Successfully wrote {image_file} to {device}.")
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")
        sys.exit(1)
    finally:
        if temp_dir:
            shutil.rmtree(temp_dir)

        os.sync()

if __name__ == "__main__":
    check_and_install_dependencies()

    image_file = input("Please enter the image file path: ")
    device = input("Please enter the device path: ")

    write_image_to_device(image_file, device)

