#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 15 21:45 2023
author: Fabio De Sicot
"""

import pymongo
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import webbrowser

# Set the plot style to dark background
plt.style.use('dark_background')

# Establish a connection to the MongoDB database
client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["Astronomy"]
collection = db["Stars"]

# Fetch the data from the MongoDB collection
stars = collection.find()

# Prepare lists to store the parameter values
magnitudes = []
distances = []
temperatures = []
ages = []
star_names = []
simbad_links = []

# Extract the parameter values from the star documents
for star in stars:
    magnitudes.append(float(star["Mag. Visual"]))
    distances.append(float(star["Distance"].split()[0]))
    temperatures.append(float(star["Temperature"].split()[0]))
    age_str = star["Age"]
    age = float(age_str.split()[0]) if age_str != '[]' else 0
    ages.append(age)
    star_names.append(star["Name"])
    simbad_links.append(star["SIMBAD"])

# Create the scatter plot with color interpolation
cmap = cm.plasma  # Choose the colormap
normalize = plt.Normalize(min(temperatures), max(temperatures))  # Normalize temperature values
colors = normalize(temperatures)  # Map temperatures to colors
scatter = plt.scatter(distances, magnitudes, c=colors, s=ages, alpha=0.8, cmap=cmap)

# Set the plot background color and grid color
ax = plt.gca()
ax.set_facecolor('black')
ax.grid(color='grey', linestyle='--')

plt.xlabel('Distance (light-years)')
plt.ylabel('Magnitude')
plt.title('Brilliant Stars')

# Variables to keep track of the clicked star name and its corresponding SIMBAD link
clicked_star = None
clicked_link = None

# Function to handle opening SIMBAD link for the clicked star
def open_simbad(event):
    global clicked_star, clicked_link
    if clicked_star is not None and event.xdata is not None and event.ydata is not None:
        for i in range(len(star_names)):
            star_name = star_names[i]
            x = distances[i]
            y = magnitudes[i]
            if star_name == clicked_star and event.xdata == x and event.ydata == y:
                webbrowser.open(clicked_link[i])
                break

# Add labels for each star (name only)
for i in range(len(star_names)):
    star_name = star_names[i]
    simbad_link = simbad_links[i]
    x = distances[i]
    y = magnitudes[i]

    def on_click(event):
        global clicked_star, clicked_link
        if isinstance(event.artist, plt.Text) and event.artist.get_text() == star_name:
            clicked_star = star_name
            clicked_link = simbad_links

    text = plt.text(x, y, star_name, color='white', fontsize=8, ha='center', va='center', picker=True)
    text.set_picker(True)
    plt.connect('pick_event', on_click)

# Connect the 'button_press_event' to open_simbad function
plt.connect('button_press_event', open_simbad)

# Add a colorbar
colorbar = plt.colorbar(scatter)
colorbar.set_label('Temperature (K)')

# Display the plot
plt.show()

